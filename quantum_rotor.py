#!/bin/python3
import numpy as np
from scipy.linalg import schur
from matplotlib import pyplot as plt
import argparse


def parse_args():
    """
    Parse the command line arguments for the script. Run `python3 
    quantum_rotor.py --help` for a list of command line options.
    """

    msg = """
        Script to diagonalize the time evolution operator for a quantum kicked
        rotor as discussed in the paper `Introduction to quantum chaos` by
        Dennis Ullmo and Steven Tomsovic. The pseudo energies are extracted and
        the histogram of their normalized level spacing is plotted and compared
        to either the statistics of the Gaussian orthogonal ensemble or the
        Poisson statistics.
    """
    parser = argparse.ArgumentParser(msg)

    msg = "The kicking strength (default: 0.5)"
    parser.add_argument("-K", help=msg, default=0.5, type=float)

    msg = "The number of discrete positions (default: 1000)"
    parser.add_argument("-M", help=msg, default=1000, type=int)

    msg = """
        Input file containing a list of pseudo energies. If present, data
        generation is omitted and the input file is used instead (default: None)
    """
    parser.add_argument("-i", "--inputfile", default=None, help=msg)

    msg = """
        The statistics with which the calculated level spacing is compared. This
        can be either `goe` for the Gaussian orthogonal ensemble or `poisson`
        for poisson statistics (default: poisson). If any other string is given,
        there will be no reference plot.
    """
    parser.add_argument("-s", "--statistics", help=msg, default="poisson")

    msg = """
        The number of bins to use in the level spacing histogram (default: 10).
    """
    parser.add_argument("-b", "--bins", default=10, help=msg, type=int)

    msg = """
        Filename of the output file. If present, the program will save the found
        pseudo energies into a data file at that location (default: None).
    """
    parser.add_argument("-o", "--outputfile", default=None, help=msg)

    msg = "Suppress graphical output"
    parser.add_argument("--no_plot", action="store_true", help=msg)

    return parser.parse_args()


def matrix_element(mm, mp, K=0.5, M=1000):
    """
    Return the matrix element of the unitary time evolution operator U as
    defined in the paper of Ullmo and Tomsovic for a given kick strength K and
    number discrete angles M at indices mm and mp.
    """
    return (
        -1
        / np.sqrt(M * 1.0j)
        * np.exp(1.0j * np.pi * (mm - mp) ** 2 / M)
        * np.exp(1.0j * K * M / 2 / np.pi * np.cos(2 * np.pi * (mm + 0.5) / M))
    )


def gen_time_evolution_operator(K=0.5, M=1000):
    """
    Create the unitary time evolution operator in matrix form for a kicking
    strength K in a spatially quantized system where M is the number of
    quantized locations.
    """
    U = np.zeros((M, M), dtype=np.complex)

    mm, mp = np.meshgrid(range(M), range(M))
    U[mm, mp] = matrix_element(mm, mp, K, M)

    return U


def get_pseudo_energies(U):
    """
    Diagonalize the time evolution operator U using a schur decomposition and
    calculate the pseudo energies from the eigenvalues.

    Returns:
        - The pseudo energies extracted from the eigenvalues of U
        - The eigenvalues of U
    """
    T = schur(U)[0]

    # The eigenvalues of the time evolution operator
    # T is already a diagonal matrix since U is unitary
    eigenvals = np.diag(T)

    pseudo_energies = np.imag(np.log(eigenvals))
    pseudo_energies.sort()

    return pseudo_energies, eigenvals


def normalize_spectrum(pseudo_energies):
    """
    Normalize the energy spectrum to have an average density of 1
    """
    # Mean level spacing
    mean_lsp = np.mean(np.diff(pseudo_energies))

    return pseudo_energies / mean_lsp


def plot_level_spacing_histogram(pseudo_energies, bins=10):
    """
    Plot the histogram of the level spacing for the given pseudo energies
    """
    histo, bin_edges = np.histogram(
        np.diff(pseudo_energies), bins=bins, density=True
    )
    centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    plt.bar(
        centers,
        histo,
        color="k",
        width=bin_edges[1] - bin_edges[0],
        fill=False,
    )
    plt.xlabel(r"$s$")
    plt.ylabel(r"$P(s)$")
    # plt.plot(centers, histo)
    return np.min(bin_edges), np.max(bin_edges)


def plot_poisson_statistics(xx):
    plt.plot(xx, np.exp(-xx))


def plot_goe_statistics(xx):
    plt.plot(xx, np.pi / 2 * xx * np.exp(-np.pi * xx ** 2 / 4))


def main():
    """
    Create a time evolution operator and diagonalize it to find the pseudo
    energies. Normalize the energy spectrum and plot the neighbor density.
    """
    args = parse_args()
    K = args.K
    M = args.M

    if args.inputfile is None:
        U = gen_time_evolution_operator(K, M)
        pseudo_energies, _ = get_pseudo_energies(U)

        if args.outputfile is not None:
            np.save(args.outputfile, pseudo_energies)
    else:
        pseudo_energies = np.load(args.inputfile)
        M = len(pseudo_energies)

    if not args.no_plot:
        # Only take the central 10% of all eigenvalues
        energy_range = range(int(M / 2 - 0.1 * M), int(M / 2 + 0.1 * M))
        normalized = normalize_spectrum(pseudo_energies[energy_range])
        x_min, x_max = plot_level_spacing_histogram(normalized, bins=args.bins)

        xx = np.linspace(x_min, x_max, 100)

        if args.statistics == "poisson":
            plot_poisson_statistics(xx)
        elif args.statistics == "goe":
            plot_goe_statistics(xx)

        plt.show()


if __name__ == "__main__":
    main()
