#!/bin/sh

python3 quantum_rotor.py -K 0.5 -M 15000 -o M_15000_K_0_5_pseudo_energies --no_plot
python3 quantum_rotor.py -K 11.6 -M 15000 -o M_15000_K_11_6_pseudo_energies --no_plot
